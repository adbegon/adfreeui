#!/usr/bin/env bash

cpan install Digest::MD5
cpan install inc::latest
cpan install Test::Moose
cpan install Types::Standard
cpan install Moo
cpan install Authen::OATH